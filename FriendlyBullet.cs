//im just your friendly neighborhood bULLET LMFAO

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
    public class FriendlyBullet
    {
        float x;
        public float y;
        float speed = 0.4f;
        Texture2D sprite;
        public FriendlyBullet(float shipX, float shipY, Texture2D texture) {
            x = shipX+15-7;
            y = shipY-14;
            sprite = texture;
        }

        public void Update(float deltaTime) {
            //move upwards, possibly hit detection, but probably not in here desu
            y -= speed*deltaTime;
        }

        public void Draw(SpriteBatch spriteBatch, float fade) {
            spriteBatch.Draw(sprite, new Vector2((int) x, (int) y), Color.White*fade);
        }

        public Rectangle GetRekt() {
            return new Rectangle((int) x,(int) y,sprite.Width,sprite.Height);
        }
    }
}

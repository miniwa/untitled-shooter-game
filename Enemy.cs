using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace shooter {
    public class Enemy {
        //ENEMIES WILL NOT HAVE ROTATION, BULLETS WILL. (instead, they'll move slightly to the left or right.)
        enum EnemyState {
            Alive, Dead, DespawnMe
        };
        public float x;
        public float y;
        float speedX;
        float speedY;
        float speedBullet;
        float speedBulletSpawn;
        int bullets;
        bool pauseBetweenBullets;
        int currentBullet = 0;
        float bulletSpread;
        float maxHealth = 100f;
        float health = 100f;
        Texture2D sprite;
        Texture2D bulletSprite;
        Rectangle gameArea;
        float lastSinceBulletShot = 0;
        Random rng = new Random();
        EnemyState state = EnemyState.Alive;
        SoundEffect dieSound;
        float ownFade = 1.0f;
        float diePos = 0;
        public Enemy(Texture2D loadSprite, Rectangle box, Texture2D loadBulletSprite, string enemyType, float yOffset, SoundEffect dieSfx, SoundEffect ronaldinhoSoccer) {
            sprite = loadSprite;
            bulletSprite = loadBulletSprite;
            gameArea = box;
            if (enemyType == "credits") {
                x = 320;
            } else {
                x = rng.Next(320+20,640+320-sprite.Width-20);
            }
            y = 0 - yOffset - sprite.Height;
            dieSound = dieSfx;
            switch (enemyType) {
                case "test": //test enemy, does basically nothing lol
                    speedX = 0.1f;
                    speedY = 0.2f;
                    speedBullet = 0.1f;
                    speedBulletSpawn = 1000f;
                    health = 10f;
                    bullets = 1;
                    bulletSpread = 1.0f;
                    pauseBetweenBullets = false;
                    break;
                case "fast":
                    speedX = 0;
                    speedY = 0.4f;
                    speedBullet = -0.2f;
                    speedBulletSpawn = 200f;
                    health = 1f;
                    bullets = 1;
                    bulletSpread = 1.0f;
                    pauseBetweenBullets = false;
                    break;
                case "tank":
                    speedX = 0.02f;
                    speedY = 0.05f;
                    speedBullet = 0.05f;
                    speedBulletSpawn = 1000f;
                    health = 200f;
                    bullets = 2;
                    bulletSpread = 0.2f;
                    pauseBetweenBullets = false;
                    break;
                case "s2s":
                    speedX = 0.4f;
                    speedY = 0.05f;
                    speedBullet = 0f;
                    speedBulletSpawn = float.MaxValue;
                    health = 1f;
                    bullets = 2;
                    bulletSpread = 0.4f;
                    pauseBetweenBullets = false;
                    break;
                case "boss1":
                    speedX = 0.05f;
                    speedY = 0.01f;
                    speedBullet = 0.1f;
                    speedBulletSpawn = 500f;
                    health = 4000f;
                    bullets = 4;
                    bulletSpread = 0.5f;
                    pauseBetweenBullets = false;
                    break;
                case "spammer":
                    speedX = 0.02f;
                    speedY = 0.2f;
                    speedBullet = 0.0f;
                    speedBulletSpawn = 400f;
                    health = 1f;
                    bullets = 5;
                    bulletSpread = 0.6f;
                    pauseBetweenBullets = true;
                    break;
                case "boss2":
                    speedX = 0.2f;
                    speedY = 0.01f;
                    speedBullet = 0.1f;
                    speedBulletSpawn = 350f;
                    health = 5000f;
                    bullets = 8;
                    bulletSpread = 0.4f;
                    pauseBetweenBullets = true;
                    break;
                case "boss3":
                    ronaldinhoSoccer.Play();
                    speedX = 0.2f;
                    speedY = 0.02f;
                    speedBullet = 0.1f;
                    speedBulletSpawn = 200f;
                    health = 6000f;
                    bullets = 6;
                    bulletSpread = 0.2f;
                    pauseBetweenBullets = true;
                    break;
                case "boss4":
                    speedX = 0.1f;
                    speedY = 0.015f;
                    speedBullet = 0.2f;
                    speedBulletSpawn = 1000f;
                    health = 7500f;
                    bullets = 8;
                    bulletSpread = 0.3f;
                    pauseBetweenBullets = false;
                    break;
                case "boss5":
                    speedX = 0.1f;
                    speedY = 0.01f;
                    speedBullet = 0.2f;
                    speedBulletSpawn = 50f;
                    health = 8000f;
                    bullets = 8;
                    bulletSpread = 0.3f;
                    pauseBetweenBullets = true;
                    break;
                case "boss6":
                    speedX = 0.1f;
                    speedY = 0.01f;
                    speedBullet = 0.35f;
                    speedBulletSpawn = 50f;
                    health = 10000f;
                    bullets = 16;
                    bulletSpread = 0.5f;
                    pauseBetweenBullets = true;
                    break;
                case "credits":
                    speedX = 0f;
                    speedY = 0.018f;
                    speedBullet = 0f;
                    speedBulletSpawn = float.MaxValue;
                    health = float.MaxValue;
                    bullets = 0;
                    bulletSpread = 0.0f;
                    pauseBetweenBullets = true;
                    break;
            }
            maxHealth = health;
        }

        public bool Update(float deltaTime, EnemyBullets enemyBullets) { //true = despawn me, false = keep goin
            switch (state) {
                case EnemyState.Alive:
                    x -= speedX*deltaTime;
                    y += speedY*deltaTime;
                    if (x < 320 || x > 320+640-sprite.Width) {
                        speedX *= -1;
                    }
                    lastSinceBulletShot += deltaTime;
                    if (lastSinceBulletShot > speedBulletSpawn) {
                        lastSinceBulletShot -= speedBulletSpawn;
                        if (y > 0-sprite.Height/4*3) {
                            if (bullets > 1) {
                                if (pauseBetweenBullets) {
                                    enemyBullets.Add(new EnemyBullet(x+sprite.Width/2-bulletSprite.Width/2, y+sprite.Height, bulletSprite, (bulletSpread*2/bullets)*(currentBullet+bullets/2) - bulletSpread*2, speedBullet+speedY));
                                    currentBullet++;
                                    if (currentBullet > bullets) {
                                        currentBullet = 0;
                                    }
                                } else {
                                    for (var i = 0; i < bullets+1; i++) { //fuck it, zero indexed
                                        enemyBullets.Add(new EnemyBullet(x+sprite.Width/2-bulletSprite.Width/2, y+sprite.Height, bulletSprite, (bulletSpread*2/bullets)*(i+bullets/2) - bulletSpread*2, speedBullet+speedY));
                                    }
                                }
                            } else {
                                enemyBullets.Add(new EnemyBullet(x+sprite.Width/2-bulletSprite.Width/2, y+sprite.Height, bulletSprite, 0.0f, speedBullet+speedY));
                            }
                        }
                    }
                    return false;
                case EnemyState.Dead:
                    if (ownFade > 0.01f) {
                        ownFade -= 0.01f*deltaTime;
                        diePos -= 0.1f*deltaTime;
                    } else {
                        state = EnemyState.DespawnMe;
                    }
                    return false;
                case EnemyState.DespawnMe:
                    return true;
                default: // ??????? not sure why i need this but vscode complains otherwise.
                    return false;
            }
            
        }

        public bool Alive() {
            if (state == EnemyState.Alive) {
                return true;
            } else {
                return false;
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, float fade) {
            switch (state) {
                case EnemyState.Alive:
                    spriteBatch.Draw(sprite, new Vector2((int) x, (int) y), Color.White*fade*ownFade);
                    break;
                case EnemyState.Dead:
                    spriteBatch.Draw(sprite, new Vector2((int) x, (int) y+diePos), Color.White*fade*ownFade);
                    break;
                case EnemyState.DespawnMe:
                    //do jack squat
                    break;
            }
        }

        public Rectangle GetRekt() { //get rekt scrub
            return new Rectangle((int) x,(int) y,sprite.Width,sprite.Height);
        }

        public void TakeDamage(float dmg) {
            health -= dmg;
            if (health < 1) {
                state = EnemyState.Dead;
                dieSound.Play();
            }
        }

        public float GetHealthNormalized() {
            return health/maxHealth;
        }
    }
}

//on a unrelated note, you might wonder, who the hell is remi?
//(IS MY GOD DAMN STUDENT CHEATING AND OUTSOURCING THEIR PROJECT?????OH GOD OH FUCK)
//remi = remilia = miniwa = """"""jack""""""
//all refer to the same person.
//also, i should change my git user.name......
//(and my email... god, thats gonna be a pain)
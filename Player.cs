using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace shooter {
    public class Player {
        float x;
        float y;
        float speed;
        bool playerHasControl = false;
        public int health = 3;
        public float power = 1.0f; // value between 1.0 and ???
        Texture2D sprite;
        Texture2D bulletSprite;
        Rectangle gameArea;
        float lastSinceBulletShot = 0;
        float timeToRespawn = 0;
        SoundEffect bulletSound;
        SoundEffect dieSound;
        public int bombs = 3;
        public List<FriendlyBullet> friendlyBullets = new List<FriendlyBullet>(); //uh, this should be fine, right? i sure fucking hope so... enemies need to get a list of the bullets available, so if they are passed the current player in a arugment, and then do Player.friendlyBullets and check for collisions there, that should be fine.. right?
        public Player(Texture2D loadSprite, Rectangle box, Texture2D loadBulletSprite, SoundEffect bulletSfx, SoundEffect dieSfx) {
            sprite = loadSprite;
            bulletSprite = loadBulletSprite;
            gameArea = box;
            bulletSound = bulletSfx;
            dieSound = dieSfx;
            x = 640-((sprite.Width+1)/2);
            y = 720;
        }

        public Vector2 getXY() {
            return new Vector2(x, y);
        }
        public float GetPower() {
            return power;
        }
        public void Update(KeyboardState keyboard, KeyboardState lastKeyboard, float deltaTime) {
            //handle funny movement, move this stuff to functions when it gets more complicated kthx
            foreach (FriendlyBullet bullet in friendlyBullets) {
                bullet.Update(deltaTime);
            }
            if (playerHasControl) {
                if (keyboard.IsKeyDown(Keys.L)) {
                    speed = (0.5f*60)/deltaTime;
                } else {
                    speed = (1.35f*60)/deltaTime;
                }
                if (keyboard.IsKeyDown(Keys.J)) {
                    if (lastKeyboard.IsKeyDown(Keys.J)) {
                        lastSinceBulletShot += deltaTime;
                        if (lastSinceBulletShot > 200/power) { //idea behind this is that autofire should be slower than mashing the button, i average 11kps so lets say a 200ms wait for autofire
                            friendlyBullets.Add(new FriendlyBullet(x+3, y, bulletSprite));
                            bulletSound.Play();
                            lastSinceBulletShot = 0;
                        }
                    } else {
                        lastSinceBulletShot = 0;
                        bulletSound.Play();
                        friendlyBullets.Add(new FriendlyBullet(x+3, y, bulletSprite));
                    }
                }
                if (keyboard.IsKeyDown(Keys.W)) {
                    if (gameArea.Contains(x, y-speed)) {
                        y -= speed;
                    } else {
                        y = gameArea.Top;
                    }
                }
                if (keyboard.IsKeyDown(Keys.S)) {
                    if (gameArea.Contains(x, y+speed+sprite.Height)) {
                        y += speed;
                    } else {
                        y = gameArea.Bottom-sprite.Height;
                    }
                }
                if (keyboard.IsKeyDown(Keys.A)) {
                    if (gameArea.Contains(x-speed, y)) {
                        x -= speed;
                    } else {
                        x = gameArea.Left;
                    }
                }
                if (keyboard.IsKeyDown(Keys.D)) {
                    if (gameArea.Contains(x+speed+sprite.Width, y)) {
                        x += speed;
                    } else {
                        x = gameArea.Right-sprite.Width;
                    }
                }
            } else {
                if (timeToRespawn <= 0) {
                    if (y > 600) {
                        y -= 0.1f*deltaTime;
                    } else {
                        //y = 600; //nah. looks ugly. +-1px doesnt really matter, anyway?
                        playerHasControl = true;
                    }
                } else {
                    timeToRespawn -= deltaTime;
                }
                
            }
            if (power > 2.5f) {
                power = 2.5f;
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, float fade) {
            spriteBatch.Draw(sprite, new Vector2((int) x, (int) y), Color.White*fade);
            List<FriendlyBullet> deleteThis = new List<FriendlyBullet>();
            foreach (FriendlyBullet bullet in friendlyBullets) { // you might ask, remi, why is this in the _DRAW_ function? my answer is FUCK YOU I DO WHAT I WANT
                if (bullet.y > -1*bulletSprite.Height) {
                    bullet.Draw(spriteBatch, fade);
                } else {
                    deleteThis.Add(bullet);
                }
            }
            foreach (FriendlyBullet bullet in deleteThis) {
                friendlyBullets.Remove(bullet);
            }
        }

        public Rectangle GetRekt() { //get rekt scrub
            return new Rectangle((int) x,(int) y,sprite.Width,sprite.Height);
        }

        public Rectangle GetHitbox() { //only works with NewShip
            return new Rectangle((int) x+17, (int) y+36, 3, 3);
        }

        public List<FriendlyBullet> GetBullets() {
            return friendlyBullets;
        }

        public void RemoveBullet(FriendlyBullet delet) {
            friendlyBullets.Remove(delet);
        }

        public int GetHealth() {
            return health;
        }

        public bool GetShouldCollide() {
            if (playerHasControl) {
                return true;
            } else {
                return false;
            }
        }
        
        public bool YouDied() { // true on gameover, false on anything else
            health--;
            if (health < 0) {
                //prevent die sound from being spammed and hurting your ears
                return false;
            } else if (health < 1) {
                //gam ovar :(
                dieSound.Play();
                return true;
            } else {
                //explosion animation
                dieSound.Play();
                x = 640-((sprite.Width+1)/2);
                y = 720;
                playerHasControl = false;
                timeToRespawn = 1000;
                power = 1.0f;
                bombs = 3;
                return false;
            }
        }
    }
}

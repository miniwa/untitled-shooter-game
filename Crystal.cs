using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
    public class Crystal
    {
        float x;
        public float y;
        float speed = 0.05f;
        public Texture2D sprite;
        public string type; //honestly, using enums is a pain..
        public Crystal(float shipX, float shipY, Texture2D texture, string leType) {
            x = shipX;
            y = shipY;
            sprite = texture;
            type = leType;
            if (x > 320+640-sprite.Width) {
                x = 320+640-sprite.Width;
            } else if (x < 320+sprite.Width) {
                x = 320+sprite.Width;
            }
        }
        public void Update(float deltaTime) {
            y += speed*deltaTime;
        }

        public void Draw(SpriteBatch spriteBatch, float fade) {
            spriteBatch.Draw(sprite, new Vector2((int) x, (int) y), Color.White*fade);
        }

        public Rectangle GetRekt() {
            return new Rectangle((int) x, (int) y,sprite.Width,sprite.Height);
        }
    }
}

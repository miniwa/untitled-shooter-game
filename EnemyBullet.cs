using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
    public class EnemyBullet
    {
        float x;
        public float y;
        float speedX = 0.0f;
        float speedY = 0.4f;
        public Texture2D sprite;
        public EnemyBullet(float shipX, float shipY, Texture2D texture, float customX, float customY) {
            x = shipX;
            y = shipY;
            sprite = texture;
            speedX = customX;
            speedY = customY;
        }

        public void Update(float deltaTime) {
            //move upwards, possibly hit detection, but probably not in here desu
            y += speedY*deltaTime;
            x += speedX*deltaTime;
            if (x < 320 || x > 320+640-sprite.Width) {
                speedX *= -1;
            }
        }

        public void Draw(SpriteBatch spriteBatch, float fade) {
            spriteBatch.Draw(sprite, new Vector2((int) x, (int) y), Color.White*fade);
        }

        public Rectangle GetRekt() {
            return new Rectangle((int) x, (int) y,sprite.Width,sprite.Height);
        }
    }
}

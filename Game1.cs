﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Newtonsoft.Json;
using System.Net.Http;

namespace shooter {
    public class Game1 : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        enum GameState {
            Loading, Menu, Game, Paused, Results, None, Exit
        };
        enum FadeState {
            None, Out, In
        };
        GameState gameState = GameState.Loading;
        List<Texture2D> textures = new List<Texture2D>();
        List<string> texturesS = new List<string>();
        List<Texture2D> backgrounds = new List<Texture2D>();
        List<string> backgroundsS = new List<string>();
        List<Song> music = new List<Song>();
        List<string> musicS = new List<string>();
        List<SoundEffect> sfx = new List<SoundEffect>();
        List<string> sfxS = new List<string>();
        List<dynamic> levels = new List<dynamic>(); //json decoding apparently gives you a dynamic. huh.
        List<string> levelsS = new List<string>();
        Texture2D LoadBg;
        Texture2D Load;
        string dataDir;
        float fade = 1.0f;
        int hoverState = 0;
        FadeState fadeState = FadeState.None;
        GameState fadeGameState = GameState.None;
        Vector2[] fgParticles = new Vector2[] {new Vector2(0,0), new Vector2(0,0)};
        MouseState mouse = Mouse.GetState();
        MouseState lastMouse = Mouse.GetState();
        KeyboardState keeb = Keyboard.GetState();
        KeyboardState lastKeeb = Keyboard.GetState();
        Random rng = new Random();
        Rectangle gameArea = new Rectangle(320,0,640,720);
        Player player;
        List<Enemy> enemies = new List<Enemy>();
        Background background;
        float deltaTime;
        EnemyBullets enemyBullets;
        float levelTime;
        int levelTimeInt;
        string level;
        dynamic levelData;
        bool levelBlocked = false;
        List<Crystal> crystals = new List<Crystal>();
        Score score;
        Enemy currentBoss;
        bool levelSelectOn = false;
        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = false;
        }
        Texture2D resultsBg;
        private static readonly HttpClient client = new HttpClient(); //all the http stuff was stolen from stackoverflow
        string highscores = "0E0E0E0E0E0E0E0E0E0E";
        bool working = false;
        bool fps = false;
        bool cheats = false;
        Texture2D[] numbers;
        string[] funnyMessages = new string[] {"Fun fact: Press F8 to adjust the music volume.", "Fun fact: Press F7 to toggle some kind of not-really-fps counter?", "i regrert", "plastic is cute", "press POGCHAMP to POGCHAMP", "please do not abuse the scoring server", "owo", "linux tech tip says : \"install gentoo\"", "can you believe i put combo scoring in this game? yeah, fuck you too.", "slap on that flex tape, fix the bugs", "play sounds vortex instead", "gjtao approved", "valuable developement time was put into making these messages", "trans rights", "stop playing this game, go get hype for arcaea 3.0 instead", "https://www.youtube.com/watch?v=PqJTTEqB5WU", "give me headpats please i beg you", "poggers", "if you reach level 5 you're legally a hunter", "tired of being straight? play this game and become a lesbian", "final fantasy 7 remake darts", "HAHAHAHA, RONALDINHO SOCCER!", "fuck me in the eth0 alexsi", "monogame quality framework", "two bald men are extremely sleepy: the game", "PEKOPEKOPEKOPEKOPEKOPEKO", "fuck unity", "fuck node.js", "php best language", "aight thats enough random messages"};
        protected override void Initialize() {
            Console.WriteLine("Shooter game - starting");
            Console.WriteLine(funnyMessages[rng.Next(0,funnyMessages.Length)]);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.ApplyChanges();
            MediaPlayer.Volume = 0.5f;
            string wherearewenow = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            dataDir = wherearewenow + "/UntitledShooterGame/";
            Console.WriteLine("Data directory: " + dataDir);
            //Console.WriteLine("Reminder to remi that you fucking hardcoded the data directory SO REMEMBER TO UNDO THAT BEFORE RELEASING");
            LoadBg = Texture2D.FromStream(GraphicsDevice, File.Open(dataDir+"/loading/Loading.png", FileMode.Open)); //hardcode loading stuff - we need this to show up before starting the actual loading...
            Load = Texture2D.FromStream(GraphicsDevice, File.Open(dataDir+"/loading/Loader.png", FileMode.Open));
            string[] files = Directory.GetFiles(dataDir+"/backgrounds/");
            for (int i = 0; i < files.Length; i++) {
                backgroundsS.Add(Path.GetFileName(files[i]).Split(".png")[0]);
            }
            files = Directory.GetFiles(dataDir+"/music");
            for (int i = 0; i < files.Length; i++) {
                musicS.Add(Path.GetFileName(files[i]).Split(".ogg")[0]);
            }
            files = Directory.GetFiles(dataDir+"/sfx");
            for (int i = 0; i < files.Length; i++) {
                sfxS.Add(Path.GetFileName(files[i]).Split(".wav")[0]);
            }
            files = Directory.GetFiles(dataDir+"/textures");
            for (int i = 0; i < files.Length; i++) {
                texturesS.Add(Path.GetFileName(files[i]).Split(".png")[0]);
            }
            files = Directory.GetFiles(dataDir+"/levels");
            for (int i = 0; i < files.Length; i++) {
                levelsS.Add(Path.GetFileName(files[i]).Split(".json")[0]);
            }
            Console.WriteLine("Files to load: " + (backgroundsS.Count + musicS.Count + sfxS.Count + texturesS.Count + levelsS.Count + "\n"));
            base.Initialize();
        }

        protected override void LoadContent() {spriteBatch = new SpriteBatch(GraphicsDevice);} // press f to pay resepcc

        protected override void Update(GameTime gameTime) {
            lastMouse = mouse;
            mouse = Mouse.GetState();
            lastKeeb = keeb;
            keeb = Keyboard.GetState();
            deltaTime = gameTime.ElapsedGameTime.Milliseconds;
            if (keeb.IsKeyDown(Keys.F8) && !lastKeeb.IsKeyDown(Keys.F8)) {
                switch (MediaPlayer.Volume) {
                    case 0.5f:
                        MediaPlayer.Volume = 0.25f;
                        break;
                    case 0.25f:
                        MediaPlayer.Volume = 0.0f;
                        break;
                    case 0.0f:
                        MediaPlayer.Volume = 0.5f;
                        break;
                }
            }
            if (keeb.IsKeyDown(Keys.F7) && !lastKeeb.IsKeyDown(Keys.F7) && gameState != GameState.Loading) {
                fps = !fps;
            }
            if (keeb.IsKeyDown(Keys.F6) && !lastKeeb.IsKeyDown(Keys.F6) && !cheats) {
                cheats = true;
                Console.WriteLine("Cheats enabled - Score publishing disabled until game restart");
            }
            handleFade(deltaTime);
            switch (gameState) {
                case GameState.Loading:
                    int loadProgress = backgrounds.Count+textures.Count+sfx.Count+music.Count+levels.Count;
                    int total = backgroundsS.Count+texturesS.Count+sfxS.Count+musicS.Count+levelsS.Count;
                    if (loadProgress == total) {
                        fadeState = FadeState.Out;
                        fadeGameState = GameState.Menu;
                        break;
                    } else {
                        loadStuff(loadProgress, total); //men remi, är det inte jävla ineffektivt att ladda en sak per frame, iallafall om du har massvis med saker?
                    }                                   //...jo, men annars får vi inte se den snygga laddningskärmen, för att det går för fort.
                    break;                              //med andra ord: its a fweature™
                case GameState.Menu:
                    updateMenuButtons();
                    updateMenuAnimation();
                    if (levelSelectOn) {
                        if (keeb.IsKeyDown(Keys.D1) || keeb.IsKeyDown(Keys.D2) || keeb.IsKeyDown(Keys.D3) || keeb.IsKeyDown(Keys.D4) || keeb.IsKeyDown(Keys.D5) || keeb.IsKeyDown(Keys.D6)) {
                            getSfx("PauseClick").Play();
                            fadeState = FadeState.Out;
                            fadeGameState = GameState.Game;
                            levelSelectOn = false;
                        }
                        if (keeb.IsKeyDown(Keys.D1)) {
                            level = "1";
                            background = new Background(getTex("bg", "ScrollBg-1"), getTex("bg", "ScrollBg-1-B"));
                        } else if (keeb.IsKeyDown(Keys.D2)) {
                            level = "2";
                            background = new Background(getTex("bg", "ScrollBg-2"), getTex("bg", "ScrollBg-2-B"));
                        } else if (keeb.IsKeyDown(Keys.D3)) {
                            level = "3";
                            background = new Background(getTex("bg", "ScrollBg-3"), getTex("bg", "ScrollBg-3-B"));
                        } else if (keeb.IsKeyDown(Keys.D4)) {
                            level = "4";
                            background = new Background(getTex("bg", "ScrollBg-4"), getTex("bg", "ScrollBg-4-B"));
                        } else if (keeb.IsKeyDown(Keys.D5)) {
                            level = "5";
                            background = new Background(getTex("bg", "ScrollBg-5"), getTex("bg", "ScrollBg-5-B"));
                        } else if (keeb.IsKeyDown(Keys.D6)) {
                            level = "6";
                            background = new Background(getTex("bg", "ScrollBg-6"), getTex("bg", "ScrollBg-6-B"));
                        } //else if .... else if .... else if... IT KEEPS GOING
                    }
                    break;
                case GameState.Game:
                    //something
                    if (keeb.IsKeyDown(Keys.Escape) && !lastKeeb.IsKeyDown(Keys.Escape)) {
                        setState(GameState.Paused);
                        break;
                    }
                    player.Update(keeb, lastKeeb, deltaTime); //update player _BEFORE_ enemies, otherwise player overlaps bullets (bAD)
                    if (keeb.IsKeyDown(Keys.K) && !lastKeeb.IsKeyDown(Keys.K) && (player.bombs > 0 || cheats)) {
                        for (int i = 0; i < enemies.Count; i++) {
                            if (enemies[i].y > 0) { //i _should_ expose the enemy sprite so i can do 0 - sprite.height... but will anyone really care? this is a super easy fix, and writing this comment took longer than the time it would take to write the fix, so that probably says something.
                                enemies[i].TakeDamage(50f); // we cant just insta-kill the bosses, so...
                            }
                        }
                        enemyBullets = new EnemyBullets();
                        player.bombs -= 1;
                    }
                    List<Enemy> deleteMe = new List<Enemy>();
                    foreach (Enemy enemy in enemies) {
                        if (enemy.Update(deltaTime, enemyBullets) == true) {
                            deleteMe.Add(enemy);
                        }
                    }
                    foreach (Crystal crystal in crystals) {
                        crystal.Update(deltaTime);
                    }
                    foreach (Enemy delet in deleteMe) {
                        int amount = rng.Next(1,5);
                        for (int i = 0; i < amount; i++) {
                            int type = rng.Next(0,2);
                            crystals.Add(new Crystal(delet.x+rng.Next(-48,48), delet.y+rng.Next(-48,48), type == 0 ? getTex("tex", "PowerCrystal") : getTex("tex", "ScoreCrystal"), type == 0 ? "power" : "score"));
                        }
                        enemies.Remove(delet);
                    }
                    if (currentBoss != null) {
                        if (currentBoss.Update(deltaTime, enemyBullets) == true) {
                            int amount = rng.Next(20,50);
                            for (int i = 0; i < amount; i++) {
                                int type = rng.Next(0,2);
                                crystals.Add(new Crystal(currentBoss.x+rng.Next(-48,48), currentBoss.y+rng.Next(-48,48), type == 0 ? getTex("tex", "PowerCrystal") : getTex("tex", "ScoreCrystal"), type == 0 ? "power" : "score"));
                            }
                            currentBoss = null;
                        }
                    }
                    enemyBullets.Update(deltaTime);
                    updateLevel();
                    background.Update(deltaTime);
                    checkCollisions();
                    score.Update(deltaTime);
                    if (player.health < 1) {
                        fadeState = FadeState.Out;
                        fadeGameState = GameState.Results;
                        resultsBg = getTex("bg", "HighScores-Dead");
                    }
                    break;
                case GameState.Paused:
                    if (keeb.IsKeyDown(Keys.Escape) && !lastKeeb.IsKeyDown(Keys.Escape)) {
                        setState(GameState.Game);
                        break;
                    }
                    if (keeb.IsKeyDown(Keys.Q)) {
                        fadeState = FadeState.Out;
                        fadeGameState = GameState.Menu;
                        break;
                    }
                    break;
                case GameState.Results:
                    if (keeb.IsKeyDown(Keys.Escape) && !lastKeeb.IsKeyDown(Keys.Escape)) {
                        fadeGameState = GameState.Menu;
                        fadeState = FadeState.Out;
                        break;
                    }
                    break;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied); //what kinda sick fuck decided not to use transparent textures by default???
            switch (gameState) {                                                      //ja detta var en av grejorna jag googlade för vad in i helsike?
                case GameState.Loading:
                    spriteBatch.Draw(LoadBg, new Vector2(0,0), Color.White*fade);
                    spriteBatch.Draw(Load, new Rectangle(0,720-64, getLoadingProgress(), 64), Color.White*fade);
                    break;
                case GameState.Menu:
                    spriteBatch.Draw(getTex("bg", "Menu-Background"), new Vector2(0,0), Color.White*fade);
                    drawMenuAnimation();
                    spriteBatch.Draw(getTex("bg", "Menu-Foreground"), new Vector2(0,0), Color.White*fade);
                    drawMenuButtons();
                    spriteBatch.Draw(getTex("tex", "Cursor"), mouse.Position.ToVector2(), Color.White*fade);
                    if (levelSelectOn) {
                        spriteBatch.Draw(getTex("tex", "PressAKeyToSelectALevel"), new Vector2(1005,404), Color.White*fade);
                    }
                    break;
                case GameState.Game:
                    //something
                    drawGame(gameTime);
                    break;
                case GameState.Paused:
                    drawGame(gameTime);
                    spriteBatch.Draw(getTex("bg", "GamePaused"), new Vector2(0), Color.White*fade);
                    spriteBatch.Draw(getTex("tex", "Cursor"), mouse.Position.ToVector2(), Color.White*fade);
                    break;
                case GameState.Results:
                    spriteBatch.Draw(resultsBg, new Vector2(0,0), Color.White*fade);
                    if (resultsBg.Height == 721) { //resized "dead" texture to 721px, because i cant think of a better way to check which one we're using without more variables
                        string drawMe = ((int) score.score).ToString().PadLeft(7, '0');
                        for (var i = 0; i < 7; i++) {
                            spriteBatch.Draw(numbers[int.Parse(drawMe[i].ToString())], new Vector2(280+588+36*i, 108), new Color(255, 255, 255)*fade);
                        }
                    }
                    for (int o = 0; o < 10; o++) {
                        string drawMe = int.Parse(highscores.Split("E")[o].Split("E")[0]).ToString().PadLeft(7, '0');
                        for (var i = 0; i < 7; i++) { //assume the player isnt good enough to get a 8-digit score (or uh, it being possible at all)
                            spriteBatch.Draw(numbers[int.Parse(drawMe[i].ToString())], new Vector2(320+630+36*i, 228+47*o), new Color(255, 255, 255)*fade);
                        }
                    }
                    break;
            }
            if (fps) {
                string drawMe = (1000f/deltaTime/62.5f*100f).ToString().PadLeft(3, '0');
                for (var i = 0; i < 3; i++) {
                    spriteBatch.Draw(numbers[int.Parse(drawMe[i].ToString())], new Vector2(1172+36*i, 720-32-4), new Color(255, 255*(1000f/deltaTime/62.5f), 255*(1000f/deltaTime/62.5f)));
                }
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        void drawGame(GameTime gameTime) { //blur background > background > crystals > (player > player bullets) > (enemies > enemy bullets) > HUD
            background.Draw(spriteBatch, fade, player.getXY());
            
            List<Crystal> deleteThisCrystal = new List<Crystal>();
            foreach (Crystal crystal in crystals) {
                if (crystal.y > 720) {
                    deleteThisCrystal.Add(crystal);
                } else {
                    crystal.Draw(spriteBatch, fade);
                }
            }
            foreach (Crystal deleteCrystal in deleteThisCrystal) {
                crystals.Remove(deleteCrystal);
            }
            player.Draw(gameTime, spriteBatch, fade);
            List<Enemy> deleteThisEnemy = new List<Enemy>();
            foreach (Enemy enemy in enemies) {
                if (enemy.y > 720) {
                    deleteThisEnemy.Add(enemy);
                    score.ResetCombo();
                } else {
                    enemy.Draw(gameTime, spriteBatch, fade);
                }
            }
            foreach (Enemy deleteEnemy in deleteThisEnemy) {
                enemies.Remove(deleteEnemy);
            }
            if (currentBoss != null) {
                if (currentBoss.y > 720) {
                    score.ResetCombo();
                    currentBoss = null;
                } else {
                    currentBoss.Draw(gameTime, spriteBatch, fade);
                    spriteBatch.Draw(getTex("tex", "BossBar"), new Rectangle(320, 0, (int) Math.Ceiling(640*currentBoss.GetHealthNormalized()), 64), Color.White*fade);
                }
            }
            enemyBullets.Draw(spriteBatch, fade);
            spriteBatch.Draw(getTex("bg", "Border"), new Vector2(0,0), Color.White*fade);
            spriteBatch.Draw(getTex("tex", "KeyboardGuide"), new Vector2(8,720-120), Color.White*fade);
            spriteBatch.Draw(getTex("tex", "RightPanel"), new Vector2(320+640,0), Color.White*fade);
            for (int i = 0; i < player.GetHealth(); i++) {
                spriteBatch.Draw(getTex("tex", "Heart"), new Vector2(320+640+33+45*i,233), Color.White*fade);
            }
            for (int i = 0; i < player.bombs; i++) {
                spriteBatch.Draw(getTex("tex", "Bomb"), new Vector2(320+640+33+48*i,385), Color.White*fade);
            }
            score.Draw(spriteBatch, fade);
            if (level == "7" && levelTime > 133.5) {
                spriteBatch.Draw(getTex("tex", "RonaldinhoSoccer"), new Vector2(0,0), Color.White*fade*Math.Clamp(((levelTime-133.5f)/2f), 0f, 1f));
            }
        }

        void checkCollisions() {
            if (level != "7") {
                List<EnemyBullet> eBullets = enemyBullets.GetBullets();
                if (player.GetShouldCollide()) {
                    for (var i = 0; i < eBullets.Count; i++) {
                        if (player.GetHitbox().Intersects(eBullets[i].GetRekt()) && !cheats) {
                            player.YouDied();
                            if (player.GetHealth() != 0) {
                                score.Die();
                            }
                            enemyBullets = new EnemyBullets();
                        }
                    }
                }
                for (var i = 0; i < enemies.Count; i++) {
                    if (enemies[i].GetRekt().Intersects(player.GetHitbox()) && !cheats) {
                        player.YouDied();
                        score.Die();
                        enemyBullets = new EnemyBullets();
                    }
                }
                List<FriendlyBullet> fBullets = player.GetBullets();
                for (var i = 0; i < enemies.Count; i++) { //'cording to someone, for loops are faster than foreach, and here we're gonna be checking a lotta stuff.
                    for (var o = 0; o < fBullets.Count; o++) { //yes, i guess i should be consistant and use them everywhere, but foreachs are nice, okay?
                        if (enemies[i].GetRekt().Intersects(fBullets[o].GetRekt()) && enemies[i].Alive()) {
                            enemies[i].TakeDamage(10f*player.GetPower());
                            player.RemoveBullet(fBullets[o]);
                            score.Add(player.GetPower());
                        }
                    }
                }
                if (currentBoss != null) {
                    if (currentBoss.GetRekt().Intersects(player.GetHitbox()) && !cheats) {
                        player.YouDied();
                        score.Die();
                        enemyBullets = new EnemyBullets();
                    }
                    for (var o = 0; o < fBullets.Count; o++) {
                        if (currentBoss.GetRekt().Intersects(fBullets[o].GetRekt()) && currentBoss.Alive()) {
                            currentBoss.TakeDamage(10f*player.GetPower());
                            player.RemoveBullet(fBullets[o]);
                            score.Add(player.GetPower());
                        }
                    }
                }
            }
            for (var i = 0; i < crystals.Count; i++) {
                if (crystals[i].GetRekt().Intersects(player.GetRekt())) {
                    getSfx("Pickup").Play();
                    if (crystals[i].type == "score") {
                        score.Add(player.GetPower());
                    } else {
                        player.power += 0.05f;
                    }
                    crystals.Remove(crystals[i]);
                }
            }
        }

        void updateLevel() {
            if (level == "7" && levelTime > 140) {
                resultsBg = getTex("bg", "HighScores-Dead");
                fadeState = FadeState.Out;
                fadeGameState = GameState.Results;
            }
            if (levelBlocked == false) {
                levelTime += deltaTime/1000;
            } else if (enemies.Count < 1 && currentBoss == null) {
                levelBlocked = false;
            }
            if (Math.Floor(levelTime) > levelTimeInt) { //new data!
                levelTimeInt = (int) Math.Floor(levelTime);
                if (levelData[levelTimeInt.ToString()] != null) {
                    if ((levelData[levelTimeInt.ToString()][0] == 1 || levelData[levelTimeInt.ToString()][0] == 2) && (enemies.Count > 0 || currentBoss != null)) {
                        levelBlocked = true;
                        levelTimeInt--;
                    } else if (levelData[levelTimeInt.ToString()][0] == 2) {
                        if (level == "bossrush") {
                            resultsBg = getTex("bg", "HighScores-Dead");
                            fadeState = FadeState.Out;
                            fadeGameState = GameState.Results;
                        } else {
                            MediaPlayer.Stop();
                            level = ((int) int.Parse(level) + (int) 1).ToString();
                            MediaPlayer.Play(getSong("Stage" + level));
                            levelTime = 0f;
                            levelTimeInt = 0;
                            levelData = getLevel(level);
                            background = new Background(getTex("bg", "ScrollBg-" + level), getTex("bg", "ScrollBg-" + level + "-B"));
                            if (player.health < 3) {
                                player.health++;
                            }
                        }
                    } else {
                        for (var i = 0; i < levelData[levelTimeInt.ToString()][1].Count; i++) {
                        dynamic enemyData = levelData[levelTimeInt.ToString()][1][i];
                            for (var o = 0; o < (int) enemyData["count"]; o++) { //yes, because of the fucking json parser, you need to cast _every single one of these_
                                addEnemy((string) enemyData["sprite"], (string) enemyData["bsprite"], (string) enemyData["type"], (bool) enemyData["boss"], (float) ((int) enemyData["spread"])*50*o);
                            }
                        }
                    }
                }
            }
        }

        void addEnemy(string sprite, string bsprite, string type, bool boss, float yOffset) {
            if (boss) {
                currentBoss = new Enemy(getTex("tex", sprite), gameArea, getTex("tex", bsprite), type, yOffset, getSfx("EnemyDestroy"), getSfx("RAnnouncer"));
            } else {
                enemies.Add(new Enemy(getTex("tex", sprite), gameArea, getTex("tex", bsprite), type, yOffset, getSfx("EnemyDestroy"), null));
            }
        }

        Texture2D getTex(string type, string tex) {
            if (type == "tex") {
                return textures[texturesS.IndexOf(tex)];
            } else if (type == "bg") {
                return backgrounds[backgroundsS.IndexOf(tex)];
            } else {
                return textures[texturesS.IndexOf("Ronaldinho")]; //bruh moment
            }
        }

        Song getSong(string sng) {
            return music[musicS.IndexOf(sng)];
        }

        SoundEffect getSfx(string sfxx) {
            return sfx[sfxS.IndexOf(sfxx)];
        }

        dynamic getLevel(string level) {
            return levels[levelsS.IndexOf(level)];
        }

        int getLoadingProgress() {
            float lulw = ((float)backgrounds.Count+(float)textures.Count+(float)sfx.Count+(float)music.Count)/((float)backgroundsS.Count+(float)texturesS.Count+(float)sfxS.Count+(float)musicS.Count)*(float)1280;
            return (int) lulw;
        }

        void loadStuff(int progress, int total) {
            Console.CursorLeft = 0;
            Console.CursorTop = Console.CursorTop-1;
            if (progress >= backgroundsS.Count+texturesS.Count+sfxS.Count+musicS.Count) {
                Console.WriteLine("Loading levels " + (progress-backgroundsS.Count-texturesS.Count-sfxS.Count-musicS.Count) + "/" + (levelsS.Count-1) + "                ");
                levels.Add(JsonConvert.DeserializeObject(File.ReadAllText(dataDir+"/levels/"+levelsS[progress-backgroundsS.Count-texturesS.Count-sfxS.Count-musicS.Count]+".json")));
                //load levels
            } else if (progress >= backgroundsS.Count+texturesS.Count+sfxS.Count) {
                Console.WriteLine("Loading music " + (progress-backgroundsS.Count-texturesS.Count-sfxS.Count) + "/" + (musicS.Count-1) + "                ");
                music.Add(Song.FromUri(musicS[progress-backgroundsS.Count-texturesS.Count-sfxS.Count], new Uri(dataDir+"/music/"+musicS[progress-backgroundsS.Count-texturesS.Count-sfxS.Count]+".ogg")));
                //load music
            } else if (progress >= backgroundsS.Count+texturesS.Count) {
                Console.WriteLine("Loading sfx " + (progress-backgroundsS.Count-texturesS.Count) + "/" + (sfxS.Count-1) + "                ");
                sfx.Add(SoundEffect.FromStream(File.Open(dataDir+"/sfx/"+sfxS[progress-backgroundsS.Count-texturesS.Count]+".wav", FileMode.Open)));
                //load sfx
            } else if (progress >= backgroundsS.Count) {
                Console.WriteLine("Loading textures " + (progress-backgroundsS.Count) + "/" + (texturesS.Count-1) + "                ");
                textures.Add(Texture2D.FromStream(GraphicsDevice, File.Open(dataDir+"/textures/"+texturesS[progress-backgroundsS.Count]+".png", FileMode.Open)));
                //load textures
            } else {
                Console.WriteLine("Loading backgrounds " + progress + "/" + (backgroundsS.Count-1) + "                ");
                backgrounds.Add(Texture2D.FromStream(GraphicsDevice, File.Open(dataDir+"/backgrounds/"+backgroundsS[progress]+".png", FileMode.Open)));
                //load backgrounds
            }
        }

        async void setState(GameState state) { //thats right fuckers we asyncronous now!! watch this break the entire game!!!!!!
            switch (gameState) {
                case GameState.Loading:
                    numbers = new Texture2D[] {getTex("tex", "Number0"), getTex("tex", "Number1"), getTex("tex", "Number2"), getTex("tex", "Number3"), getTex("tex", "Number4"), getTex("tex", "Number5"), getTex("tex", "Number6"), getTex("tex", "Number7"), getTex("tex", "Number8"), getTex("tex", "Number9")};
                    break;
                case GameState.Menu:
                    //stop playing music (only in case we're going to the game, highscores and what not we wouldent wanna change music for .. . .)
                    if (state == GameState.Game) {
                        MediaPlayer.Stop();
                    }
                    break;
                case GameState.Game:
                    //was considering stopping music when you die, but nah.
                    //who knows...
                    break;
                case GameState.Paused:
                    //mostly having this here in case i do end up needing it.
                    getSfx("PauseClick").Play();
                    break;
                case GameState.Results:
                    //....
                    break;
            }
            switch (state) {
                case GameState.Loading:
                    Console.WriteLine("What?");
                    break;
                case GameState.Menu:
                    //play menu music, and what not.
                    fadeState = FadeState.In;
                    levelSelectOn = false;
                    MediaPlayer.IsRepeating = true;
                    MediaPlayer.Play(getSong("Menu"));
                    gameState = GameState.Menu;
                    break;
                case GameState.Game:
                    if (gameState != GameState.Paused) {
                        fadeState = FadeState.In;
                        player = new Player(getTex("tex", "NewShip"), gameArea, getTex("tex", "FriendlyBullet"), getSfx("Bullet"), getSfx("PlayerDestroy"));
                        levelTime = 0f;
                        levelTimeInt = 0;
                        levelData = getLevel(level);
                        levelBlocked = false;
                        enemyBullets = new EnemyBullets();
                        MediaPlayer.Play(getSong("Stage" + level));
                        enemies = new List<Enemy>();
                        crystals = new List<Crystal>();
                        currentBoss = null;
                        score = new Score(numbers);
                    }
                    gameState = GameState.Game;
                    break;
                case GameState.Paused:
                    getSfx("PauseClick").Play();
                    gameState = GameState.Paused;
                    break;
                case GameState.Results:
                    if (working) { //oh god, because of async fuckery, i need to do this ?!?!?!
                        break;     //this runs more than once i kid you not FUCK ASYNC i m ean im probably doing something bad (calling setstate more than once) and itsp rob not actually async
                    }              // but ???????????????? oh well TODO: fix whatever makes this run >1
                    working = true;
                    if (gameState == GameState.Game && !cheats) {
                        var result = await client.GetStringAsync("https://miniwa.space/geimu/add_score.php?lol=" + (int) score.score);
                        Console.WriteLine(result);
                    }
                    highscores = await client.GetStringAsync("https://miniwa.space/geimu/get_scores.php");
                    fadeState = FadeState.In;
                    gameState = GameState.Results;
                    working = false;
                    break;
                case GameState.Exit:
                    Exit();
                    break;
            }
        }

        void handleFade(float deltaTime) {
            switch (fadeState) {
                case FadeState.None:
                    break;
                case FadeState.Out:
                    if (fade <= 0.0f) {
                        fadeState = FadeState.None;
                        if (fadeGameState != GameState.None) {
                            setState(fadeGameState);
                            fadeGameState = GameState.None;
                        }
                    } else {
                        fade -= 0.001f*deltaTime;
                    }
                    break;
                case FadeState.In:
                    if (fade >= 1.0f) {
                        fadeState = FadeState.None;
                        if (fadeGameState != GameState.None) {
                            setState(fadeGameState);
                            fadeGameState = GameState.None;
                        }
                    } else {
                        fade += 0.001f*deltaTime;
                    }
                    break;
            }
        }

        void updateMenuAnimation() {
            fgParticles[1].X += 0.3f*deltaTime;
            fgParticles[1].Y += 0.15f*deltaTime;
            if (fgParticles[1].X > 1280) {fgParticles[1].X = 0;}
            if (fgParticles[1].Y > 720) {fgParticles[1].Y = 0;}
            fgParticles[0].X -= 0.15f*deltaTime;
            fgParticles[0].Y -= 0.3f*deltaTime;
            if (fgParticles[0].X < -1280) {fgParticles[0].X = 0;}
            if (fgParticles[0].Y < -720) {fgParticles[0].Y = 0;}
        }

        void drawMenuAnimation() {
            spriteBatch.Draw(getTex("bg", "Menu-Particle1"), fgParticles[0], Color.White*fade);
            spriteBatch.Draw(getTex("bg", "Menu-Particle1"), new Vector2(fgParticles[0].X, fgParticles[0].Y + 720), Color.White*fade);
            spriteBatch.Draw(getTex("bg", "Menu-Particle1"), new Vector2(fgParticles[0].X + 1280, fgParticles[0].Y), Color.White*fade);
            spriteBatch.Draw(getTex("bg", "Menu-Particle1"), new Vector2(fgParticles[0].X + 1280, fgParticles[0].Y + 720), Color.White*fade);
            spriteBatch.Draw(getTex("bg", "Menu-Particle2"), fgParticles[1], Color.White*fade);
            spriteBatch.Draw(getTex("bg", "Menu-Particle2"), new Vector2(fgParticles[1].X, fgParticles[1].Y - 720), Color.White*fade);
            spriteBatch.Draw(getTex("bg", "Menu-Particle2"), new Vector2(fgParticles[1].X - 1280, fgParticles[1].Y), Color.White*fade);
            spriteBatch.Draw(getTex("bg", "Menu-Particle2"), new Vector2(fgParticles[1].X - 1280, fgParticles[1].Y - 720), Color.White*fade);
        }

        void updateMenuButtons() {
            if (lastMouse.LeftButton != ButtonState.Pressed && mouse.LeftButton == ButtonState.Pressed) {
                if (new Rectangle(1000,300,82,49).Contains(mouse.Position)) {
                    getSfx("PauseClick").Play();
                    fadeState = FadeState.Out;
                    level = "1"; 
                    background = new Background(getTex("bg", "ScrollBg-1"), getTex("bg", "ScrollBg-1-B"));
                    fadeGameState = GameState.Game;
                } else if (new Rectangle(1000,370,209,42).Contains(mouse.Position)) {
                    //level select button pressed
                    getSfx("PauseClick").Play();
                    levelSelectOn = true;
                } else if (new Rectangle(1000,440,185,42).Contains(mouse.Position)) {
                    //boss rush button pressed
                    getSfx("PauseClick").Play();
                    fadeState = FadeState.Out;
                    level = "bossrush"; 
                    background = new Background(getTex("bg", "ScrollBg-BossRush"), getTex("bg", "ScrollBg-BossRush-B"));
                    fadeGameState = GameState.Game;
                } else if (new Rectangle(1000,510,208,49).Contains(mouse.Position)) {
                    //high scores button pressed
                    getSfx("PauseClick").Play();
                    resultsBg = getTex("bg", "HighScores-Normal");
                    fadeGameState = GameState.Results;
                    fadeState = FadeState.Out;
                } else if (new Rectangle(1000,580,73,42).Contains(mouse.Position)) {
                    //exit button pressed
                    getSfx("PauseClick").Play();
                    fadeState = FadeState.Out;
                    fadeGameState = GameState.Exit;
                }
            } else {
                if (new Rectangle(1000,300,82,49).Contains(mouse.Position) && hoverState != 1) {
                    getSfx("Hover").Play();
                    hoverState = 1;
                } else if (new Rectangle(1000,370,209,42).Contains(mouse.Position) && hoverState != 2) {
                    getSfx("Hover").Play();
                    hoverState = 2;
                } else if (new Rectangle(1000,440,185,42).Contains(mouse.Position) && hoverState != 3) {
                    getSfx("Hover").Play();
                    hoverState = 3;
                } else if (new Rectangle(1000,510,208,49).Contains(mouse.Position) && hoverState != 4) {
                    getSfx("Hover").Play();
                    hoverState = 4;
                } else if (new Rectangle(1000,580,73,42).Contains(mouse.Position) && hoverState != 5) {
                    getSfx("Hover").Play();
                    hoverState = 5;
                } else if (!new Rectangle(1000,300,209,322).Contains(mouse.Position)) {
                    hoverState = 0;
                }
            }
        }
        void drawMenuButtons() {
            /*
            [10:48 AM] Miniwa♥: 0.8547
                                1.0
                                0.279
            [10:48 AM] Miniwa♥: 36px arial, #fff, #cd00e1 2px 1.5px
            [11:07 AM] Miniwa♥: 0.1395
                                1.0
                                0.302
            not even related, but i also used corbel light somewhere???
            */
            spriteBatch.Draw(getTex("tex", new Rectangle(1000,300,82,49).Contains(mouse.Position) ? "Play-Focused" : "Play-Unfocused"), new Vector2(1000, 300), Color.White*fade);
            spriteBatch.Draw(getTex("tex", new Rectangle(1000,370,209,42).Contains(mouse.Position) ? "LevelSelect-Focused" : "LevelSelect-Unfocused"), new Vector2(1000, 370), Color.White*fade);
            spriteBatch.Draw(getTex("tex", new Rectangle(1000,440,185,42).Contains(mouse.Position) ? "BossRush-Focused" : "BossRush-Unfocused"), new Vector2(1000, 440), Color.White*fade);
            spriteBatch.Draw(getTex("tex", new Rectangle(1000,510,208,49).Contains(mouse.Position) ? "HighScores-Focused" : "HighScores-Unfocused"), new Vector2(1000, 510), Color.White*fade);
            spriteBatch.Draw(getTex("tex", new Rectangle(1000,580,73,42).Contains(mouse.Position) ? "Exit-Focused" : "Exit-Unfocused"), new Vector2(1000, 580), Color.White*fade);
        }
    }
}

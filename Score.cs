using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
    public class Score
    {
        public float score = 0;
        float lastScore = 0;
        float combo = 0;
        Texture2D[] numbers;
        float[] numberFade = new float[7] {128f, 128f, 128f, 128f, 128f, 128f, 128f};
        public Score(Texture2D[] textures) {
            numbers = textures;
        }
        public void Add(float power) {
            lastScore = score;
            combo++;
            score += 1*combo*power;
        }

        public void ResetCombo() {
            combo = 0;
        }

        public void Die() {
            score -= (score/4)*3;
        }

        public void Update(float deltaTime) {
            string scoreS = int.Parse(Math.Floor(score).ToString()).ToString().PadLeft(7, '0');
            string lastScoreS = int.Parse(Math.Floor(lastScore).ToString()).ToString().PadLeft(7, '0');
            for (var i = 0; i < 7; i++) {
                if (scoreS[i] != lastScoreS[i]) {
                    numberFade[i] = 0;
                } else {
                    numberFade[i] += 0.5f*deltaTime;
                }
            }
            lastScore = score;
        }
        public void Draw(SpriteBatch spriteBatch, float fade) {
            string drawMe = int.Parse(Math.Floor(score).ToString()).ToString().PadLeft(7, '0'); //it makes _perfect_ sense
            for (var i = 0; i < 7; i++) { //assume the player isnt good enough to get a 8-digit score (or uh, it being possible at all)
                spriteBatch.Draw(numbers[int.Parse(drawMe[i].ToString())], new Vector2(320+640+32+36*i, 82), new Color(255, 128+(int) numberFade[i], 255)*fade);
            }
        }
    }
}
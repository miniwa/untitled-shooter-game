using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
    public class Background
    {
        float y = 0;
        Texture2D level;
        Texture2D levelBlur;
        float speed = 1.2f; //12 = ~8.3m for a full scroll, so make sure levels are like. 6 minutes long? idk...

        public Background(Texture2D initialTexture, Texture2D initialTextureBlur) {
            level = initialTexture;
            levelBlur = initialTextureBlur;
        }

        public void Update(float deltaTime) {
            y += (speed)/deltaTime;
        }

        public void Draw(SpriteBatch spriteBatch, float fade, Vector2 PlayerPos) {
            spriteBatch.Draw(levelBlur, new Vector2(320, (int) y-6000+720), Color.White*fade);
            spriteBatch.Draw(level, new Vector2(300+(PlayerPos.X-320)/640*40, (int) y-6000+760+PlayerPos.Y/720*40), Color.White*fade);
        }

        public void ChangeBackground(Texture2D newTexture, Texture2D newTextureBlur) {
            level = newTexture;
            levelBlur = newTextureBlur;
            y = 0;
        }
    }
}

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

//...why did i make this its own thing? not really sure. oh well.

namespace shooter
{
    public class EnemyBullets
    {
        List<EnemyBullet> bullets = new List<EnemyBullet>();

        public void Add(EnemyBullet bullet) {
            bullets.Add(bullet);
        }

        public List<EnemyBullet> GetBullets() {
            return bullets;
        }

        public void Update(float deltaTime) {
            foreach (EnemyBullet bullet in bullets) {
                bullet.Update(deltaTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch, float fade) {
            List<EnemyBullet> deleteThis = new List<EnemyBullet>();
            foreach (EnemyBullet bullet in bullets) { // you might ask, remi, why is this in the _DRAW_ function? my answer is go read the other comment gtfo lmao
                if (bullet.y > 720+bullet.sprite.Height) {
                    deleteThis.Add(bullet);
                } else {
                    bullet.Draw(spriteBatch, fade);
                }
            }
            foreach (EnemyBullet bullet in deleteThis) {
                bullets.Remove(bullet);
            }
        }
    }
}